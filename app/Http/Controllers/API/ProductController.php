<?php


namespace App\Http\Controllers\API;

use App\Models\Producto;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    /**
     *  constructor para asegurar que solo los usuarios autenticados puedan acceder a los métodos del controlador.
     */
    public function __construct()
    {
        $this->middleware('auth:api');
        
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Obtener el usuario autenticado
        $user = Auth::guard('api')->user();
        
        // Lógica para mostrar todos los productos, tal vez según el usuario autenticado
        $productos = Producto::all();

        return response()->json(['productos' => $productos]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Obtener el usuario autenticado
        $user = Auth::guard('api')->user();
        
        // Lógica para mostrar un producto específico
        $producto = Producto::with('categoria', 'proveedor')->find($id);

        if (!$producto) {
            return response()->json(['message' => 'Producto no encontrado'], 404);
        }

        return response()->json(['producto' => $producto]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Obtener el usuario autenticado
        $user = Auth::guard('api')->user();
    
        // Validar la solicitud
        $request->validate([
            'nombre' => 'required',
            'precio' => 'required',
            'categoria_id' => 'required',
            'proveedor_id' => 'required',
            // Otros campos requeridos
        ]);

        // Crear un nuevo producto
        $producto = Producto::create([
            'nombre' => $request->input('nombre'),
            'precio' => $request->input('precio'),
            'categoria_id' => $request->input('categoria_id'),
            'proveedor_id' => $request->input('proveedor_id'),
            // Otros campos del producto
        ]);
        // obtenemos el producto creado recientemente con sus detalles 
        $newProducto = Producto::with('categoria', 'proveedor')->find($producto->id);;

        return response()->json(['producto' => $newProducto], 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Obtener el producto a actualizar
        $producto = Producto::find($id);

        // Verificar si el producto existe
        if (!$producto) {
            return response()->json(['message' => 'Producto no encontrado'], 404);
        }

        // Validar la solicitud
        $request->validate([
            'nombre' => 'required',
            'precio' => 'required',
            'categoria_id' => 'required',
            'proveedor_id' => 'required'
            // Puedes agregar más reglas de validación según tus necesidades
        ]);

        // Verificar si el usuario autenticado es el propietario del producto
        /*if (Auth::guard('api')->user()->id !== $producto->user_id) {
            return response()->json(['message' => 'No tienes permisos para actualizar este producto'], 403);
        }*/

        // Actualizar el producto con los datos proporcionados en la solicitud
        $producto->update($request->all());

         // Cargar la relación de categoría
        $producto->load('categoria');

        // Cargar la relación de proveedor
        $producto->load('proveedor');

        return response()->json(['producto' => $producto, 'message' => 'Producto actualizado correctamente'], 200);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Obtener el usuario autenticado
        $user = Auth::guard('api')->user();
        
        // Buscar el producto
        $producto = Producto::find($id);

        if (!$producto) {
            return response()->json(['message' => 'Producto no encontrado'], 404);
        }

        // Eliminar el producto
        $producto->delete();

        return response()->json(['message' => 'Producto eliminado correctamente'], 200);
    }
}

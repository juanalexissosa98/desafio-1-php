<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Categoria;
use App\Models\Proveedor;
use App\Models\Producto;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

         \App\Models\User::factory()->create([
             'name' => 'Admin User',
             'email' => 'admin@gmail.com',
             'password' => bcrypt(123456),
         ]);

        //CREACION DE ALGUNAS CATEGORIAS
        Categoria::create(['nombre' => 'Electrodomésticos']);
        Categoria::create(['nombre' => 'Electrónicos']);
        Categoria::create(['nombre' => 'Hogar']);
        Categoria::create(['nombre' => 'Muebles']);
        Categoria::create(['nombre' => 'Moda']);
        
        
        

        // CREACION DE PROVEEDORES
        Proveedor::create(['nombre' => 'Electrodomésticos Py', 'direccion' => 'Asunción']);
        Proveedor::create(['nombre' => 'Electrónicos SA', 'direccion' => 'Asunción']);
        Proveedor::create(['nombre' => 'Hogar y Más', 'direccion' => 'Asunción']);
        Proveedor::create(['nombre' => 'Muebles del Sur', 'direccion' => 'Encarnación']);
        Proveedor::create(['nombre' => 'Moda y Estilo', 'direccion' => 'Asunción']);
       

        //CREACION DE PODUCTOS

        
        Producto::create(['nombre' => 'producto1', 'precio' => 1, 'categoria_id' => 1, 'proveedor_id' => 1]);
        Producto::create(['nombre' => 'producto2', 'precio' => 100000, 'categoria_id' => 1, 'proveedor_id' => 1]);
        Producto::create(['nombre' => 'producto3', 'precio' => 200000, 'categoria_id' => 1, 'proveedor_id' => 1]);
        Producto::create(['nombre' => 'producto4', 'precio' => 250000, 'categoria_id' => 1, 'proveedor_id' => 1]);
        Producto::create(['nombre' => 'producto5', 'precio' => 300000, 'categoria_id' => 2, 'proveedor_id' => 2]);
        Producto::create(['nombre' => 'producto6', 'precio' => 105000, 'categoria_id' => 3, 'proveedor_id' => 2]);
        Producto::create(['nombre' => 'producto7', 'precio' => 35000, 'categoria_id' => 2, 'proveedor_id' => 2]);
        Producto::create(['nombre' => 'producto8', 'precio' => 78000, 'categoria_id' => 3, 'proveedor_id' => 3]);
        Producto::create(['nombre' => 'producto9', 'precio' => 85000, 'categoria_id' => 4, 'proveedor_id' => 3]);
        Producto::create(['nombre' => 'producto10', 'precio' => 550000, 'categoria_id' => 4, 'proveedor_id' => 4]);
        Producto::create(['nombre' => 'producto11', 'precio' => 95000, 'categoria_id' => 5, 'proveedor_id' => 4]);
        Producto::create(['nombre' => 'producto12', 'precio' => 126500, 'categoria_id' => 4, 'proveedor_id' => 5]);
        Producto::create(['nombre' => 'producto13', 'precio' => 100100, 'categoria_id' => 5, 'proveedor_id' => 5]);
        Producto::create(['nombre' => 'producto14', 'precio' => 900000, 'categoria_id' => 3, 'proveedor_id' => 4]);

        

    }
}

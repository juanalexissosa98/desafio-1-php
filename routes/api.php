<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\ProductController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(UserController::class)->group(function(){
    Route::post('login','loginUser');
});

// Rutas para el controlador de usuarios
Route::controller(UserController::class)->group(function(){
    Route::get('user','getUserDetail');
    Route::get('logout','userLogout');
})->middleware('auth:api');

// Rutas para el controlador de productos
Route::controller(ProductController::class)->group(function () {
    Route::get('productos', 'index');
    Route::get('productos/{id}', 'show');
    Route::post('productos', 'store');
    Route::put('productos/{id}', 'update');
    Route::delete('productos/{id}', 'destroy');
})->middleware('auth:api');